// Application entrypoint.

// Load up the application styles
require("../styles/application.scss");

// Render the top-level React component
import React from 'react';
import ReactDOM from 'react-dom';
import DrawChart from './draw-chart.jsx';
import ChartData from './source-data'

 ReactDOM.render(<DrawChart data={ChartData} width={500} height={500} />, document.getElementById('chartRoot'));
 