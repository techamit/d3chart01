import React, {Component} from 'react';

class DrawChart extends Component {

  // unique id for SVG element
  uniqueId = 'svg' + Math.random().toString().replace('.','');

  // valid chart data
  // eleminate 'count' has zero
  validData = this.props.data.filter( d => d.count > 0);

  root = d3.hierarchy({
    children: this.validData
  })
  .sum(function (d) {
    return d.count;
  }); 

  pack = d3.pack()
  .size([this.props.width, this.props.height])
  .padding(1.5);

  drawCircles(){
    var leaves  = this.pack( this.root ).leaves();

    return(
        leaves.map( v=> {
            return(
              <g key={this.uniqueKey()} className="node" transform={"translate(" + v.x + "," + v.y + ")"}>
              <circle r={v.r}>
              </circle>
              <text>
                <tspan x="0" y="0" className="titleText1">{v.data.qno}</tspan>
                <tspan x="0" y="20" className="titleText2">{v.data.count}</tspan>
              </text>
              <title>
                {v.data.qno} {v.data.count}
              </title>
              </g>
            );
          }
        )
    );
  }

  // Generates unique keys
  uniqueKey(){
    return ( 'key' + Math.random().toString().replace('.','') );
  }

  render() {
    this.state = {
      svgId: this.uniqueId,
      data: this.props.data
    }

    return (
      <svg width={500} height={500}>
      {this.drawCircles()}
      </svg>
    );
  }
   
}

export default DrawChart;